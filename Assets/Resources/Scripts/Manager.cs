using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public CharacterTopDownController controller;
    private Pathfinder pf;
    private List<Vector2> raycastDirections = new List<Vector2>();

    public LinkedList<Command> _commands = new LinkedList<Command>();
    private Command _currentCommand = null;

    private Vector2 lastWaitngPoint;
    public Vector2 table;

    public GameObject workersHub;
    private List<Worker> workers = new List<Worker>();

    public Player caughtPlayer = null;

    // Start is called before the first frame update
    void Start()
    {
        table = transform.position;
        controller = GetComponent<CharacterTopDownController>();
        pf = GetComponent<Pathfinder>();

        foreach(Worker w in workersHub.GetComponentsInChildren<Worker>())
        {
            workers.Add(w);
        }

        raycastDirections.Add(Vector2.up);
        raycastDirections.Add(Vector2.down);
        raycastDirections.Add(Vector2.left);
        raycastDirections.Add(Vector2.right);
    }

    // Update is called once per frame
    void Update()
    {
        ListenForCommands();
    }

    private void FixedUpdate()
    {
        ProcessCommand();
    }

    private void ListenForCommands()
    {
        FindPlayerRaycast();
        CatchPlayer();
        GetCalls();
        GoBack();
    }

    private void ProcessCommand()
    {
        if (_currentCommand != null && !_currentCommand.IsFinished()) return;
        if (_commands.Count == 0) return;
        _currentCommand = _commands.First.Value;
        _commands.RemoveFirst();
        _currentCommand.Execute();
    }

    public void FindPathAndMove(Vector2 target, bool interuptable)
    {
        // find path to curInterest
        pf.InitPathfinding(lastWaitngPoint, target);
        List<Vector2> pathPoints = pf.FindPath();
        // go to curInterest
        if (pathPoints.Count > 0)
        {
            lastWaitngPoint = target;
            LineRenderer lr = GetComponent<LineRenderer>();
            lr.positionCount = Math.Max(pathPoints.Count, 1);
            lr.SetPosition(0, transform.position);
            for (int i = 1; i < pathPoints.Count; i++)
            {
                lr.SetPosition(i, pathPoints[i]);
                Command com = new MoveManagerCommand(this, (pathPoints[i]), interuptable);
                _commands.AddLast(com);
            }
        }
    }

    private void FindPlayerRaycast()
    {
        if (caughtPlayer == null)
        {
            RaycastHit2D hit;
            for (int i = 0; i < raycastDirections.Count; i++)
            {
                hit = Physics2D.Raycast(transform.position, raycastDirections[i], 10f);
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject.CompareTag("Player"))
                    {
                        //if (hit.collider.transform.position.x == Math.Floor(hit.collider.transform.position.x)
                        //    && hit.collider.transform.position.y == Math.Floor(hit.collider.transform.position.y))
                        //{
                        _commands.Clear();
                        _commands.AddFirst(new MoveManagerCommand(this
                            , new Vector2((float)Math.Floor(hit.collider.transform.position.x + .5f)
                                , (float)Math.Floor(hit.collider.transform.position.y + .5f))
                            , false));
                        break;
                        //}
                    }
                }
            }
        }
    }

    private void CatchPlayer()
    {
        Player player = Player.GetInstance();
        if (caughtPlayer == null && Vector2.Distance(player.transform.position, transform.position) < 0.5f)
        {
            player.SetCaught(this);
            caughtPlayer = player;
            _commands.Clear();
            _commands.AddFirst(new PathfinderManagerCommand(this, caughtPlayer.StartPoint, false));
        }
    }

    private void GetCalls()
    {
        foreach(Worker w in workers)
        {
            if (w.isTriggered && !w.isManaged)
            {
                w.isManaged = true;
                _commands.AddLast(new PathfinderManagerCommand(this, w.callPoint.position, false));
            }
        }
    }

    private void GoBack()
    {
        if (_commands.Count == 0 && (_currentCommand == null || _currentCommand.IsFinished()))
        {
            if (Vector2.Distance(transform.position, table) > 0.05f)
            {
                pf.InitPathfinding(lastWaitngPoint, table);
                List<Vector2> pathPoints = pf.FindPath();
                //_commands.AddLast(new LookAroundManagerCommand(this));
                _commands.AddLast(new MoveManagerCommand(this, pathPoints[1], true));
            }
        }
    }
}
