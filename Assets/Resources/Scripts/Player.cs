using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    static Player instance;
    public bool isPaused = false;

    public Vector2 StartPoint;
    public Dictionary<InterType, bool> progress;

    CharacterTopDownController controller;
    private Vector2 _movement;

    [Header("Interact")]
    public Interactable inter;
    public Manager caughtBy = null;

    public static Player GetInstance()
    {
        return instance;
    }

    void Start()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;

        StartPoint = new Vector2(0, 0);
        controller = GetComponent<CharacterTopDownController>();
        inter = null;
        progress = new Dictionary<InterType, bool>();
        foreach(InterType inter in Enum.GetValues(typeof(InterType)))
        {
            progress.Add(inter, false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            if (caughtBy == null)
            {
                UnpausedUpdate();
            }
            else
            {
                CaughtUpdate();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                isPaused = false;
                Time.timeScale = 1;
                _ = SceneManager.UnloadSceneAsync(1);
            }
            else
            {
                isPaused = true;
                Time.timeScale = 0;
                SceneManager.LoadScene(1, LoadSceneMode.Additive);
            }
        }
    }

    private void FixedUpdate()
    {
        if (_movement != Vector2.zero)
        {
            controller.MoveStep(_movement);
        }
    }

    private void UnpausedUpdate()
    {
        _movement = new Vector2(Input.GetAxis("Horizontal"), 0);
        if (_movement == Vector2.zero)
        {
            _movement = new Vector2(0, Input.GetAxis("Vertical"));
        }

        if (inter != null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                inter.StartInteraction();
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                inter.StopInteraction();
            }
        }
    }

    private void CaughtUpdate()
    {
        _movement = new Vector2(
                (float)(Math.Floor(caughtBy.transform.position.x + 0.5) - Math.Floor(transform.position.x + 0.5))
                , (float)(Math.Floor(caughtBy.transform.position.y + 0.5) - Math.Floor(transform.position.y + 0.5))
            );
    }

    public void SetCaught(Manager manager)
    {
        caughtBy = manager;
    }

    public void SetFree()
    {
        caughtBy = null;
    }
}
