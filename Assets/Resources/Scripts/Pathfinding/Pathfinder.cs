using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Pathfinder : MonoBehaviour
{
    public Tilemap Floor;
    public Tilemap Walls;
    List<Tile> activeTiles;
    List<Tile> visitedTiles;
    Tile startTile;
    Tile targetTile;

    public void InitPathfinding(Vector2 start, Vector2 interest)
    {
        activeTiles = new List<Tile>();
        visitedTiles = new List<Tile>();
        startTile = new Tile(transform.position);
        targetTile = new Tile(interest);
        startTile.SetDistanceTo(targetTile.position);
        activeTiles.Add(startTile);
    }

    private List<Tile> GetAccessableTiles(Tile fromTile, Tile toTile)
    {
        float newCost = fromTile.cost + 100;
        List<Tile> possibleTiles = new List<Tile>()
        {
            new Tile(fromTile.position + Vector2.down, fromTile, newCost ),
            new Tile(fromTile.position + Vector2.up, fromTile, newCost ),
            new Tile(fromTile.position + Vector2.left, fromTile, newCost ),
            new Tile(fromTile.position + Vector2.right, fromTile, newCost )
        };
        possibleTiles.ForEach(tile => tile.SetDistanceTo(toTile.position));
        return possibleTiles
                .Where(tile => tile.IsInTilemap(Floor))
                .Where(tile => !tile.IsInTilemap(Walls))
                .ToList();
    }

    public List<Vector2> FindPath()
    {
        while (activeTiles.Any())
        {
            Tile checkTile = activeTiles.OrderBy(x => x.costDistance).First();
            if (checkTile.position == targetTile.position)
            {
                List<Vector2> result = new List<Vector2>();
                Tile trace = checkTile;
                while (trace != null)
                {
                    result.Insert(0, trace.position);
                    trace = trace.parent;
                }
                return result;
            }
            visitedTiles.Add(checkTile);
            activeTiles.Remove(checkTile);

            List<Tile> walkableTiles = GetAccessableTiles(checkTile, targetTile);
            foreach (Tile walkableTile in walkableTiles)
            {
                if (visitedTiles.Any(x => x.position == walkableTile.position))
                {
                    continue;
                }

                if (activeTiles.Any(x => x.position == walkableTile.position))
                {
                    Tile existingTile = activeTiles.First(x => x.position == walkableTile.position);
                    if (existingTile.costDistance > checkTile.costDistance)
                    {
                        activeTiles.Remove(existingTile);
                        activeTiles.Add(walkableTile);
                    }
                }
                else
                {
                    activeTiles.Add(walkableTile);
                }
            }
        }
        Debug.Log("No Path Found!");
        return new List<Vector2>();
    }
}
