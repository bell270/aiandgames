using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Tile
{
	public Vector2 position { get; set; }
	public float cost { get; set; }
	public float distance { get; set; }
	public float costDistance => cost + distance;
	public Tile parent { get; set; }

	public Tile(Vector2 pos)
    {
		position = pos;
		cost = 0;
		distance = 0;
		parent = null;
    }

	public Tile(Vector2 pos, Tile parent, float cost)
    {
		position = pos;
		this.cost = cost;
		distance = 0;
		this.parent = parent;
    }
	public void SetDistanceTo(Vector2 target)
	{
		distance = Math.Abs(target.x - position.x) + Math.Abs(target.y - position.y);
	}
	
	public bool IsInTilemap(Tilemap tm)
    {
		return tm.HasTile(Vector3Int.FloorToInt(position) + new Vector3Int(-1, -1, 0));
	}
}
