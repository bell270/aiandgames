using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Interactable : MonoBehaviour
{
    public InterType type;
    public bool isActive = false;
    public bool isFinished = false;
    public bool isProcessed = false;
    public float progressSpeed = 5f;
    public float decreaseSpeed = 1f;
    [SerializeField]private float _progress = 0f;
    private float _progressTimer = 0f;
    public Slider ProgressBar;
    private Animator pbAnimator;


    // Start is called before the first frame update
    void Start()
    {
        pbAnimator = ProgressBar.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            _progressTimer += Time.deltaTime;
            if ( _progressTimer >= 1f)
            {
                _progressTimer = 0f;
                _progress += progressSpeed;
                if (_progress >= 100)
                {
                    _progress = 100;
                    isFinished = true;
                }
            }
        }
        else if (!isFinished)
        {
            _progressTimer += Time.deltaTime;
            if (_progressTimer >= 1f)
            {
                _progressTimer = 0f;
                _progress -= decreaseSpeed;
                if (_progress <= 0)
                {
                    _progress = 0;
                }
            }
        }
        ProgressBar.value = _progress;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isFinished && collision.CompareTag("Player"))
        {
            collision.GetComponent<Player>().inter = this;
            pbAnimator.SetBool("isUnfaded", true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!isFinished && collision.CompareTag("Player"))
        {
            collision.GetComponent<Player>().inter = null;
            pbAnimator.SetBool("isUnfaded", false);
            StopInteraction();
        }
    }

    public void StartInteraction()
    {
        isActive = true;
        _progressTimer = 0f;
    }

    public void StopInteraction()
    {
        isActive = false;
        _progressTimer = 0f;
    }
}
