using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CharacterTopDownController : MonoBehaviour
{
    private Rigidbody2D _rb;

    [Header("Movement Variables")]
    public float speedMultiplier = 0f;
    public float rotationSpeed = 0f;
    public float maxMagnitude = 0f;
    public Tilemap Walls;
    [Space(10)]

    public bool isMoving = false;
    private Vector2 _movement = Vector2.zero;
    private Vector2 _lookAt = Vector2.zero;

    [Header("Command Queue")]
    private Queue<Vector2> destinationQueue = new Queue<Vector2>();

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (destinationQueue.Count != 0 && !isMoving)
        {
            Vector2 dest = destinationQueue.Dequeue();
            MoveTo(dest);
        }
    }

    public void Move(Vector2 movement)
    {
        _rb.MovePosition(_rb.position + speedMultiplier * movement * Time.fixedDeltaTime);
    }

    public void LookAt(Vector2 lookAt)
    {
        Vector2 direction = lookAt - new Vector2(transform.position.x, transform.position.y);
        float angle = Vector2.SignedAngle(transform.right, direction);
        float angleDiff = angle - _rb.rotation;
        float newAngle = (_rb.rotation + rotationSpeed * angle * Time.fixedDeltaTime) % 360;
        _rb.rotation = newAngle;
    }

    public void MoveQueue(Vector2 destination)
    {
        destinationQueue.Enqueue(destination);
    }

    public bool MoveStep(Vector2 direction)
    {
        return MoveTo(new Vector2((int)_rb.position.x, (int)_rb.position.y) + direction.normalized);
    }

    public bool MoveTo(Vector2 moveTo)
    {
        if (!isMoving && !WallCollision(moveTo))
        {
            isMoving = true;
            StartCoroutine(MoveToCoroutine(moveTo));
            return true;
        }
        return false;
    }

    private IEnumerator MoveToCoroutine(Vector2 moveTo)
    {
        while (isMoving)
        {
            Vector2 movement = moveTo - new Vector2(transform.position.x, transform.position.y);
            _rb.MovePosition(_rb.position + speedMultiplier * movement.normalized * Time.fixedDeltaTime);
            yield return null;
            if (Vector2.Distance(_rb.position, moveTo) < 0.05f)
            {
                _rb.position = moveTo;
                isMoving = false;
            }
        }
    }

    private bool WallCollision(Vector3 checkPos) // true when wall
    {
        return Walls.HasTile(Vector3Int.FloorToInt(checkPos) + new Vector3Int(-1, -1, 0));
    }

    public void LookTo(Vector2 lookToDirection)
    {
        float angle = Vector2.SignedAngle(transform.right, lookToDirection);
        float angleDiff = angle - _rb.rotation;
        float newAngle = (_rb.rotation + rotationSpeed * angle * Time.fixedDeltaTime) % 360;
        _rb.rotation = newAngle;
    }
}
