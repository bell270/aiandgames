using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowManagerCommand : Command
{
    public readonly Manager _actor;
    public readonly Player _player;

    public FollowManagerCommand(Manager actor, Player player)
    {
        this._actor = actor;
        this._player = player;
    }

    public override void Execute()
    {
    }

    public override bool IsFinished()
    {
        return true;
    }

    public override string ToString()
    {
        return "MM " + "_" + "_" + _actor.transform.position + "_" + IsFinished();
    }
}
