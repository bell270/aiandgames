using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoveManagerCommand : Command
{
    public readonly Manager _actor;
    public readonly Vector2 _destination;
    public readonly bool _interuptable;

    public MoveManagerCommand(Manager _actor, Vector2 _dest, bool interuptable)
    {
        this._actor = _actor;
        this._destination = _dest;
        this._interuptable = interuptable;
    }

    public override void Execute()
    {
        if (_actor.caughtPlayer != null)
        {
            Debug.Log(Vector2.Distance(_actor.transform.position, _destination));
            if (Vector2.Distance(_actor.caughtPlayer.StartPoint, _destination) < 0.05f)
            {
                _actor.caughtPlayer.SetFree();
            }
            if (Vector2.Distance(_actor.table, _destination) < 0.05f)
            {
                _actor.caughtPlayer.SetFree();
                _actor.caughtPlayer = null;
            }
        }
        _actor.controller.MoveQueue(_destination);
    }

    public override bool IsFinished() {
        return (Vector3.Distance(_actor.transform.position, _destination) == 0f);
    }

    public override string ToString()
    {
        return "MM " + _destination + "_" + "_" + _actor.transform.position + "_" + IsFinished();
    }
}

