using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfinderManagerCommand : Command
{
    public readonly Manager _actor;
    public readonly Vector2 _target;
    public bool _isFinished;
    public readonly bool _interuptable;

    public PathfinderManagerCommand(Manager _actor, Vector2 _target, bool interuptable)
    {
        this._actor = _actor;
        this._target = _target;
        this._isFinished = false;
        this._interuptable = interuptable;
    }

    public override void Execute()
    {
        _actor.FindPathAndMove(_target, _interuptable);
        _isFinished = true;
    }

    public override bool IsFinished() {
        return _isFinished;
    }

    public override string ToString()
    {
        return "PFM " + _target;
    }
}
