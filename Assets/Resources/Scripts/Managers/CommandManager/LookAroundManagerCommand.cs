using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAroundManagerCommand : Command
{
    public readonly Manager _actor;
    public float _waitTimer;
    public bool _isFinished;

    public LookAroundManagerCommand(Manager actor)
    {
        this._actor = actor;
        this._waitTimer = 2f;
        this._isFinished = false;
    }

    public override void Execute()
    {
        while (_waitTimer >= 0f)
        {
            _waitTimer -= Time.deltaTime;
        }
        
    }

    public override bool IsFinished()
    {
        return _isFinished;
    }

    public override string ToString()
    {
        return "MM " + "_" + "_" + _actor.transform.position + "_" + IsFinished();
    }
}
