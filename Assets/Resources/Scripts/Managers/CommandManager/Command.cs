
[System.Serializable]
public abstract class Command
{
    public abstract void Execute();

    public abstract bool IsFinished();

    public abstract string ToString();
}
