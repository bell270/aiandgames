using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InteractManager : MonoBehaviour
{
    public List<Interactable> interactables;
    public Dictionary<InterType, int> progress;
    // Start is called before the first frame update
    void Start()
    {
        progress = new Dictionary<InterType, int>();
        interactables = new List<Interactable>(GetComponentsInChildren<Interactable>());
        foreach(Interactable inter in interactables)
        {
            if (!progress.ContainsKey(inter.type))
            {
                progress.Add(inter.type, 0);
            }
            progress[inter.type]++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Interactable inter in interactables)
        {
            if (inter.isFinished && !inter.isProcessed)
            {
                inter.isProcessed = true;
                progress[inter.type]--;
            }
        }
        if (progress.ContainsValue(0))
        {
            InterType type = progress.FirstOrDefault(x => x.Value == 0).Key;
            Debug.Log("Finish");
            Player.GetInstance().progress[type] = true;
        }
    }
}
