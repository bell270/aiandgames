using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour
{

    public Transform callPoint;
    public List<Interactable> watch;
    private Animator animator;

    public bool isTriggered;
    public bool isManaged;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        if (callPoint == null)
        {
            callPoint = GetComponentInChildren<Transform>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        bool isAlerted = false;
        foreach(Interactable inter in watch)
        {
            if (inter.isActive)
            {
                isAlerted = true;
                if (!isTriggered) CallManager();
                break;
            }
        }
        if (!isAlerted)
        {
            isTriggered = false;
            animator.SetBool("isTriggered", false);
        }
    }

    public void CallManager()
    {
        animator.SetBool("isTriggered", true);
        isTriggered = true;
        isManaged = false;
    }
}
